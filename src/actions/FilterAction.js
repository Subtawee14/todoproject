export const FILTER = "FILTER";
export const filterAction = (filter) => {
    return {
      type: FILTER,
      payLoad : filter
    };
  };