export const DONE_TODO = "DONE_TODO";
export const FETCH_TODO = "FETCH_TODO";

export const done_todo = (id) => {
  return {
    type: DONE_TODO,
    payLoad: id
  };
};

export const fetchTodo = () => {
  return {
    type: FETCH_TODO
  };
};

export const setTodo = (data) => {
  return {
    type: "SET_TODO",
    payLoad: data
  };
};
