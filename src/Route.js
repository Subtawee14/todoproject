import React from "react";
import { Route, Redirect, BrowserRouter } from "react-router-dom";
import { Layout, Menu, Breadcrumb, Icon } from "antd";
import User from "./components/User";
import UserTodo from "./components/UserTodo";
import Album from "./components/Album";
import Albumdetail from "./components/AlbumDetail";
const { Header, Content, Footer } = Layout;

const MainRouting = () => {
  const renderBreadcrumb = () => {
    return (
      <Breadcrumb style={{ margin: "16px 0" }}>
        <Breadcrumb.Item>Home</Breadcrumb.Item>
        <Breadcrumb.Item>List</Breadcrumb.Item>
        <Breadcrumb.Item>App</Breadcrumb.Item>
      </Breadcrumb>
    );
  };
  return (
    <BrowserRouter>
      <Layout style={{height : "100vh"}}>
        <Header style={{ position: "fixed", zIndex: 1, width: "100%" }}>
          <div className="logo" />
          <Menu
            theme="dark"
            mode="horizontal"
            defaultSelectedKeys={["2"]}
            style={{ lineHeight: "64px" }}
          >
            <Icon type="home" />

            <Menu.Item key="1">
              <a href="/home">User List</a>
            </Menu.Item>
          </Menu>
        </Header>
        {renderBreadcrumb()}
        <Content style={{ padding: "0 50px", marginTop: 64, overflow : "auto"}}>
          <Route exact path="/" component={User} />
          <Route path="/home" component={User} />
          <Route path="/user/:user_id/todo" component={UserTodo} />
          <Route exact path="/user/:user_id/albums" component={Album} />
          <Route path="/user/:user_id/albums/:album_id" component={Albumdetail} />
        </Content>
        <Footer style={{ textAlign: "center" , backgroundColor: "#001529" , color: "white" }}>
          Ant Design ©2018 Created by Ant UED
        </Footer>
      </Layout>
    </BrowserRouter>
  );
};

export default MainRouting;
