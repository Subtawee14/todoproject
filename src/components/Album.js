import React, { useState, useEffect } from "react";
import { Row, Col, Icon } from "antd";
import Albumdetail from "./AlbumDetail";
import { List, Typography } from "antd";
import { message, Avatar, Spin } from "antd";
const Album = (props) => {
  const [albums, setAlbums] = useState();
  const [user, setUser] = useState();
  const [Loading, setLoading] = useState(true);
  const [loadingUser, setLoadingUser] = useState(true);
  const renderAvatar = () => {
    if (user) {
      return (
        <Col span={6} style={{ textAlign: "center" }}>
          <Row>
            <Avatar size={64} icon="user" />
            <h3>{user.name}</h3>
            <h4>{user.email}</h4>
          </Row>
        </Col>
      );
    } else {
      return (
        <Col span={6} style={{ textAlign: "center" }}>
          <Avatar size={64} icon="user" />
          <h3></h3>
        </Col>
      );
    }
  };
  useEffect(() => {
    fetchUser();
    fetchData();
  }, []);
  const fetchUser = () => {
    setLoadingUser(true);
    setTimeout(() => {
      fetch(
        "https://jsonplaceholder.typicode.com/users/" +
          props.match.params.user_id
      )
        .then((response) => response.json())
        .then((data) => {
          setUser(data);
          setLoadingUser(false);
        });
    }, 1000);
  };
  const fetchData = () => {
    setLoading(true);
    setTimeout(() => {
      fetch(
        "https://jsonplaceholder.typicode.com/albums?userId=" +
          props.match.params.user_id
      )
        .then((response) => response.json())
        .then((data) => {
          setAlbums(data);
          setLoading(false);
        });
    }, 1000);
  };

  return (
    <div>
      <Row>
        {renderAvatar()}
        <Col span={18}>
          <h3 style={{ marginBottom: 16 }}>Albums</h3>
          <Spin spinning={Loading}>
            <List
              bordered
              dataSource={albums}
              renderItem={(item) => (
                <List.Item>
                  <a
                    href={
                      "/user/" +
                      props.match.params.user_id +
                      "/albums/" +
                      item.id
                    }
                  >
                    {item.title}
                  </a>
                </List.Item>
              )}
            />
          </Spin>
        </Col>
      </Row>
    </div>
  );
};

export default Album;
