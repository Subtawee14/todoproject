import React, { useState, useEffect } from "react";
import { Card, Col, Row, Spin } from "antd";
const Albumdetail = (props) => {
  const [images, setImages] = useState();
  const [Loading, SetLoading] = useState(true);
  const [user, setUser] = useState();
  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = () => {
    SetLoading(true);

    fetch(
      "https://jsonplaceholder.typicode.com/photos?albumId=" +
        props.match.params.album_id
    )
      .then((response) => response.json())
      .then((data) => {
        setImages(data);
        SetLoading(false);
      });
  };

  const fetchUser = () => {
    fetch(
      "https://jsonplaceholder.typicode.com/users/" + props.match.params.user_id
    )
      .then((response) => response.json())
      .then((data) => {
        setUser(data);
      });
  };

  const renderCard = () => {
    if (images) {
      return images.map((value, index) => {
        return (
          <Col span={6}>
            <Card
              title={value.title}
              bordered={true}
              style={{ marginBottom: 10 }}
            >
              <img src={value.url} width="100%" />
            </Card>
          </Col>
        );
      });
    } else {
      return <div>No data</div>;
    }
  };
  return (
    <Row>
      <Row>
        <h1>ALbum Detail </h1>
      </Row>

      <Row>
        <Spin size="large" spinning={Loading}>
          <div style={{ background: "#ECECEC", padding: "30px" }}>
            <Row gutter={16}>{renderCard()}</Row>
          </div>
        </Spin>
      </Row>
    </Row>
  );
};

export default Albumdetail;
