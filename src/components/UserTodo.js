import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { Table, Tag } from "antd";
import { Row, Spin, Col } from "antd";
import { Avatar, Button } from "antd";
import { Select } from "antd";
import { useDispatch } from "react-redux";
import { done_todo, fetchTodo, setTodo } from "../actions/TodoAction";
import { filterAction } from "../actions/FilterAction";
const { Option } = Select;

const UserTodo = (props) => {
  const [user, setUser] = useState();
  const filter = useSelector((state) => state.filter);
  const todo = useSelector((state) => state.todo);
  const [isLoading, setIsLoading] = useState(true);
  const dispatch = useDispatch();

  const handleChange = (value) => {
    console.log(`selected ${value}`);
    if (value == "1") {
      dispatch(filterAction(true));
    } else if (value == "2") {
      dispatch(filterAction(false));
    } else {
      dispatch(filterAction("all")); 
    }
  };
  const fetchData = () => {
    setIsLoading(true);
    fetch(
      "https://jsonplaceholder.typicode.com/todos?userId=" +
        props.match.params.user_id
    )
      .then((response) => response.json())
      .then((data) => {
        dispatch(setTodo(data));
        setIsLoading(false);
      })
      .catch((err) => {
        alert(err);
        console.log(err);
      });
  };
  useEffect(() => {
    fetchData();
    fetchUser();
  }, []);

  const columns = [{
    title : "ID",
    dataIndex : "id",
    key : "id"
  },
    {
      title: "Title",
      dataIndex: "title",
      key: "title"
    },
    {
      title: "Status",
      dataIndex: "completed",
      key: "completed",
      render: (status) => {
        return status ? (
          <Tag color="green">Done</Tag>
        ) : (
          <Tag color="red">Waiting</Tag>
        );
      }
    },
    {
      title: "Action",
      key: "id",
      dataIndex: "completed",
      render: (status, object) => {
        if (status == false) {
          return (
            <Button
              type="primary"
              onClick={() => {
                check(object.id);
              }}
            >
              Done
            </Button>
          );
        } else {
          return <div>Done</div>;
        }
      }
    }
  ];

  const check = (id) => {
    dispatch(done_todo(id));
  };

  const showTodo = () => {
    return filter == "all"
      ? todo
      : todo.filter((value) => {
          return value.completed == filter;
        });
  };

  const renderTable = () => {
    return <Table columns={columns} dataSource={showTodo()} />;
  };
  const fetchUser = () => {
    fetch(
      "https://jsonplaceholder.typicode.com/users/" + props.match.params.user_id
    )
      .then((response) => response.json())
      .then((data) => {
        setUser(data);
      });
  };
  const renderAvatar = () => {
    if (user) {
      return (
        <Col span={6}>
          <Row>
            <Avatar size={64} icon="user" />
            <h3>{user.name}</h3>
            <h4>{user.email}</h4>
          </Row>
          <Row>
            <h4>Address</h4>
            <p>{user.address.street}</p>
          </Row>
        </Col>
      );
    } else {
      return (
        <Col span={6}>
          <Avatar size={64} icon="user" />
          <h3></h3>
        </Col>
      );
    }
  };

  return (
    <Row>
      {renderAvatar()}
      <Col span={18}>
        <Row>
          <Col>
            <h2>Todo List</h2>
            <Select
              defaultValue="All"
              style={{ width: 120 }}
              onChange={handleChange}
            >
              <Option value="0">All</Option>
              <Option value="1">Done</Option>
              <Option value="2">Waiting</Option>
            </Select>
          </Col>
        </Row>
        <Spin spinning={isLoading}>{renderTable()}</Spin>
      </Col>
    </Row>
  );
};

export default UserTodo;
