import React, { useState, useEffect } from "react";
import { List, message, Avatar, Spin } from "antd";
import { Input } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { searchAction } from '../actions/SearchAction'
const { Search } = Input;
const User = () => {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);
  const name = useSelector((state) => state.search);

  const dispatch = useDispatch();

  useEffect(() => {
    fetchData();
  }, [name]);

  const fetchData = () => {
    setLoading(true);
    if (name) {
      let newArray = data.filter((value) => {
        return value.name.toLowerCase().match(name.toLowerCase());
      });
      setData(newArray);
    } else {
      fetch("https://jsonplaceholder.typicode.com/users")
        .then((response) => response.json())
        .then((data) => {
          setData(data);
        });
    }

    setLoading(false);
  };

  const search = (value) => {
    dispatch(searchAction(value));
  };
  return (
    <div className="demo-infinite-container">
      <Spin spinning={loading} />
      <Search
        placeholder="input search text"
        onChange={(e) => search(e.target.value)}
        style={{ width: 200 }}
      />

      <List
        dataSource={data}
        renderItem={(item) => (
          <List.Item key={item.id}>
            <List.Item.Meta
              avatar={
                <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
              }
              title={<a href="https://ant.design">{item.name.last}</a>}
              description={item.name}
            />
            <div>
              <a href={"/user/" + item.id + "/todo"}>Todo</a>
            </div>
            <div style={{ marginLeft: 20 }}>
              <a href={"/user/" + item.id + "/albums"}>Albums</a>
            </div>
          </List.Item>
        )}
      ></List>
    </div>
  );
};

export default User;
