import {FILTER } from "../actions/FilterAction";


export const initialFilter = "all"

export const filterReducer = (state = initialFilter, action) => {
    switch (action.type) {
      case FILTER:
        return action.payLoad;
      default:
        return state;
    }
  };