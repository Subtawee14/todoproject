import { DONE_TODO, FETCH_TODO } from "../actions/TodoAction";

const initialTodos = [];
export const todoReducer = (state = initialTodos, action) => {
  switch (action.type) {
    case DONE_TODO:
      let newArray = [...state];
      newArray.map((value) => {
        if (value.id == action.payLoad) {
          return (value.completed = true);
        }
      });
      return newArray;
    case FETCH_TODO:
      return state;
    case 'SET_TODO':
      console.log(action.payLoad)
      return action.payLoad;
    default:
      return state;
  }
};
